import pandas as pd

class Factor(pd.DataFrame):
    """
    Class representing factors. This class inherits from pandas DataFrame and implements factor operations
    """
    number = 1
    summations = 0
    multiplications = 0

    def __init__(self, distribution, i=None):
        """
        Initializes this factor as the given distribution but with the column "prob" renamed to "val"
        """
        pd.DataFrame.__init__(self, data=distribution.rename(columns={"prob": "val"}))
        if i is None:
            self.i = Factor.number
            Factor.number += 1
        else:
            self.i = i


    def reduce(self, variable, value):
        """
        A function implementing factor reduction
        """
        result = self[self[variable] == value]
        result = result.drop([variable], axis=1)
        return Factor(result, self.i)

    def marginalize(self, variable):
        """
        A function implementing variable marginalization in a factor
        """
        others = [column for column in self.columns[:-1] if column != variable]
        rows = len(self)
        result = self.groupby(others, as_index=False, sort=False).sum()
        Factor.summations += rows - len(result)
        return Factor(result, self.i)

    @staticmethod
    def product(factors):
        """
        A function implementing factor multiplication
        """
        result = factors[0]
        for i in range(1, len(factors)):
            # Get the columns in common
            common = list(result.columns.intersection(factors[i].columns))[:-1]
            # Get a new DataFrame that merges the two operands based on the columns they have in common
            result = result.merge(factors[i], on=common)
            # Compute the product values of the merged factors
            result["val"] = result["val_x"] * result["val_y"]
            result = result.drop(["val_x", "val_y"], axis=1)
            Factor.multiplications += len(result)
        return Factor(result)