"""
@Author: Joris van Vugt, Moira Berens, Leonieke van den Bulk

Entry point for the creation of the variable elimination algorithm in Python 3.
Code to read in Bayesian Networks has been provided. We assume you have installed the pandas package.

"""
from read_bayesnet import BayesNet
from variable_elim import VariableElimination

from os import listdir
import re

if __name__ == '__main__':
    networkName = "earthquake"
    heuristic = 0 # 0 for no heuristic, 1 for least incoming arcs first, 2 for contained in fewest factors first

    # The class BayesNet represents a Bayesian network from a .bif file in several variables
    net = BayesNet(f"../networks/{networkName}.bif")
    
    # These are the variables read from the network that should be used for variable elimination
    """
    print("Nodes:")
    print(net.nodes)
    print("Values:")
    print(net.values)
    print("Parents:")
    print(net.parents)
    print("Probabilities:")
    print(str(net.probabilities).replace(", ", "\n").replace(": ", ":\n"))
    """

    # Make your variable elimination code in the seperate file: 'variable_elim'. 
    # You use this file as follows:
    ve = VariableElimination(net)

    # Set the node to be queried as follows:
    query = 'Alarm'

    # The evidence is represented in the following way (can also be empty when there is no evidence): 
    evidence = {}

    # Determine your elimination ordering before you call the run function. The elimination ordering
    # is either specified by a list or a heuristic function that determines the elimination ordering
    # given the network. Experimentation with different heuristics will earn bonus points. The elimination
    # ordering can for example be set as follows:
    if heuristic == 0: # No heuristic
        elim_order = [node for node in net.nodes if node!=query and node not in evidence]
    elif heuristic == 1: # least incoming arcs first
        elim_order = [node for node in sorted(net.nodes, key=lambda node: len(net.parents[node])) if node!=query and node not in evidence]
    elif heuristic == 2: # contained in fewest factors first
        def elim_order(factors):
            remainingNodes = list(set([node for factor in factors for node in factor.columns[:-1] if node != query and node not in evidence]))
            factorsWithNode = lambda node: len([factor for factor in factors if node in factor.columns])
            return None if len(remainingNodes) == 0 else sorted(remainingNodes, key=factorsWithNode)[0]

    logCounter = max([0] + list(map(lambda file: int(re.search(f"(\d*)_log", file).group(1)), [file for file in listdir("../logs/") if file.startswith(networkName)]))) + 1

    # Call the variable elimination function for the queried node given the evidence and the elimination ordering as follows:   
    print(ve.run(query, evidence, elim_order, logName=f"../logs/{networkName}_{logCounter}_log.txt"))