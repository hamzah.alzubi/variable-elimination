"""
@Author: Joris van Vugt, Moira Berens, Leonieke van den Bulk

Class for the implementation of the variable elimination algorithm.

"""

import pandas as pd
from factor import Factor

class VariableElimination():

    def __init__(self, network):
        """
        Initialize the variable elimination algorithm with the specified network.
        Add more initializations if necessary.

        """
        self.network = network

    def run(self, query, observed, elim_order, logName="log.txt"):
        """
        Use the variable elimination algorithm to find out the probability
        distribution of the query variable given the observed variables

        Input:
            query:      The query variable
            observed:   A dictionary of the observed variables {variable: value}
            elim_order: Either a list specifying the elimination ordering
                        or a function that will determine an elimination ordering
                        given the network during the run
            logName:    The name of the log file

        Output: A variable holding the probability distribution
                for the query variable

        """
        # Start a logger
        logger = VELogger(logName, self.network, query, observed)
        logger.logInitInfo()

        # Get the factors
        factors = [Factor(distribution) for distribution in self.network.probabilities.values()]
        logger.logString("\n\nFactors:\n")
        logger.logFactors(factors)

        # Reduce observed variables
        for variable in observed:
            for i, factor in reversed(list(enumerate(factors))):
                if variable in factor.columns:
                    factors[i] = factor.reduce(variable, observed[variable])
                    logger.logString(f"Reduced f{factor.i} with observed variable {variable}={observed[variable]}\n")
                    if len(factors[i].columns) == 1:
                        del factors[i]
                    else:
                        logger.logFactors([factors[i]])

        # If the elimination ordering is a list, make it into a function for uniform treatment later
        if isinstance(elim_order, list):
            elimList = elim_order.copy()
            elim_order = lambda _: elimList.pop(0) if len(elimList)>0 else None
            logger.logString(f"Elimination ordering is static:\n{VELogger.varsToString(elimList)}\n\n")
        else:
            logger.logString("Elimination ordering is dynamic and the variable will be determined in each iteration\n\n")

        # Elimination process
        eliminationWidth = 0
        variable = elim_order(factors) # Get the next variable in the elimination ordering
        while variable is not None:
            # Get and remove the factors containing this variable
            factorsWithVariable = []
            for i in reversed(list(range(len(factors)))):
                if variable in factors[i].columns:
                    factorsWithVariable.append(factors.pop(i))
            # Create the new factor
            if len(factorsWithVariable) > 0:
                newFactor = Factor.product(factorsWithVariable)
                if len(newFactor.columns) > 2:
                    newFactor = newFactor.marginalize(variable)
                    factors.append(newFactor)
                eliminationWidth = max(len(newFactor.columns)-1, eliminationWidth)
                logger.logElimination(variable, factors, factorsWithVariable, newFactor)
            variable = elim_order(factors) # Get the next variable in the elimination ordering

        # Get final factor
        result = Factor.product(factors)
        logger.logString(f"Elimination complete. Elimination width was {eliminationWidth}\n")
        logger.logString(f"Total number of summations: {Factor.summations}\nTotal number of multiplications: {Factor.multiplications}\n\n")
        logger.logString("Final factor:\n")
        logger.logFactors([result])

        # Normalize
        result["val"] /= result["val"].sum()
        result = result.drop([column for column in list(result.columns) if column!="val" and column!=query], axis=1)
        result = pd.DataFrame(data=result.rename(columns={"val": "prob"}))
        logger.logString(f"Resulting normalized table:\n{result.to_string(index=False)}")

        return result


"""
Helper class for logging functionalities
"""
class VELogger():
    def __init__(self, fileName, network, query, observed):
        self.fileName = fileName
        self.network = network
        self.query = query
        self.observed = observed
        open(fileName, "w").close()

    def logInitInfo(self):
        with open(self.fileName, "a") as logFile:
            logFile.write(f"Query variable: {self.query}\n")

            logFile.write("\nObserved variables:\n")
            for var, val in self.observed.items():
                logFile.write(f"{var}: {val}\n")

            logFile.write("\nProduct formula:\n")
            logFile.write(f"SUM_{{{VELogger.varsToString([node for node in self.network.nodes if node != self.query])}}}")
            for i in range(len(self.network.nodes)):
                logFile.write(f"P({self.network.nodes[i]}")
                logFile.write(")" if i==0 else f"|{VELogger.varsToString([node for node in self.network.nodes[:i]])})")

            logFile.write(f"\n\nReduced product formula:\n")
            logFile.write(f"SUM_{{{VELogger.varsToString([node for node in self.network.nodes if node != self.query])}}}")
            for node, parents in self.network.parents.items():
                logFile.write(f"P({node}{'' if len(parents) == 0 else '|' + VELogger.varsToString(parents)})")

    def logFactors(self, factors, explicit=True):
        with open(self.fileName, "a") as logFile:
            for factor in factors:
                logFile.write(f"f{factor.i}({VELogger.varsToString(list(factor.columns[:-1]))})")
                if explicit:
                    logFile.write(f"=\n{factor.to_string(index=False)}\n\n")

    def logElimination(self, variable, factors, factorsWithVariable, newFactor):
        self.logString(f"Eliminated next variable in ordering: {variable}\n")
        if newFactor is not None:
            self.logString(f"New factor: SUM_{{{variable}}}")
            self.logFactors(factorsWithVariable, explicit=False)
            self.logString("\n")
            self.logFactors([newFactor])
            self.logString(f"The factors are now:\n")
            self.logFactors(factors, explicit=False)
            self.logString("\n\n")

    def logString(self, s):
        with open(self.fileName, "a") as logFile:
            logFile.write(s)

    @staticmethod
    def varsToString(variables):
        return str(variables)[1:-1].replace("'", "").replace(" ", "")